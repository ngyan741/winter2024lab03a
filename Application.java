public class Application{
	public static void main(String args[]){
		
		Student student1 = new Student();
		student1.name = "Timmy";
		student1.grade = 65;
		student1.isFirstYear = true;
		student1.height = 165.5;
		student1.firstLanguage = "English";
	
		Student student2 = new Student();
		student2.name = "Jessie";
		student2.grade = 86;
		student2.isFirstYear = false;
		student2.height = 180.0;
		student2.firstLanguage = "Francais";
		
		Student[] section4 = new Student[3];
		section4[0] = student1; 
		section4[1] = student2;
		
		section4[2] = new Student();
		section4[2].name = "Barry";
		section4[2].grade = 54;
		section4[2].isFirstYear = false;
		section4[2].height = 189.2;
		section4[2].firstLanguage = "English";
		
		System.out.println("Name: " + section4[2].name + " Grade: " + section4[2].grade + " First Year? " + section4[2].isFirstYear + " Height: " + section4[2].height + " First Language: " + section4[2].firstLanguage);
		
		//student1.introduceSelf();
		//student2.introduceSelf();
		
		//student1.study();
		//student2.study();
		
		//student1.checkFailing();
		//student2.checkFailing();
		
		
	}
}